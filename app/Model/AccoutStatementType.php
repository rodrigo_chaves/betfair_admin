<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AccoutStatementType extends Model
{
    protected $table = 'account_statement_types';
}
