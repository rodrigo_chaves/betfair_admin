<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class AccountStatement extends Model
{
    protected $table = 'account_statements';

    public function user(){
        return $this->belongsTo('App\Model\User');
    }

    public function placedFormated(){
        $carbon = Carbon::createFromFormat('Y-m-d H:i:s', $this->placed);
        return $carbon->format('Y-m-d H:i');
    }

    public function createdFormated(){
        $carbon = Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at);
        return $carbon->format('Y-m-d H:i');
    }
}
