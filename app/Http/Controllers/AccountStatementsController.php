<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\AccountStatement;
use App\Model\AccoutStatementType;
use App\Model\User;
use Validator;

class AccountStatementsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $accountStatements = AccountStatement::orderBy('created_at','DESC')->paginate(20);

        return View('accountstatements.index')->with('statements',$accountStatements);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::all();
        $accountStatementTypes = AccoutStatementType::all();
        return View('accountstatements.create')
                ->with('users',$users)
                ->with('statementTypes',$accountStatementTypes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'description' => 'required',
            'average_odds' => 'required',
            'stopped' => 'required',
            'state' => 'required',
            'rd' => 'required',
            'rc' => 'required',
            'balance' => 'required',
            'ref_id' => 'required'
        ]);

        if($validator->fails()){
            return redirect()->back()->with('errors',$validator->errors()->all());
        }
        else{
            $accountStatement = new AccountStatement;

            $accountStatement->ref_id = $request->input('ref_id');
            $accountStatement->placed = $request->input('placed');
            $accountStatement->description = $request->input('description');
            $accountStatement->average_odds = $request->input('average_odds');
            $accountStatement->stopped = $request->input('stopped');
            $accountStatement->state = $request->input('state');
            $accountStatement->rd = $request->input('rd');
            $accountStatement->rc = $request->input('rc');
            $accountStatement->balance = $request->input('balance');
            $accountStatement->user_id = $request->input('user_id');
            $accountStatement->account_statement_types_id = $request->input('account_statement_types_id');

            $accountStatement->save();

            return redirect()->route('accountstatements.index');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
