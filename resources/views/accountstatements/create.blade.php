@extends('layout.panel')

@section('content')
<div class="content">
  <div class="row">
    <div class="col-lg-12">
      @if(Session::has('errors'))
        <div class="alert alert-danger"><?= implode("<br>", Session::get('errors'))?></div>
      @endif
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Add Account Statement</h3>
        </div>
        <form action="{{route('accountstatements.store')}}" method="post">
          {{ csrf_field() }}
          <div class="box-body">

            <div class="col-lg-4">
              <div class="form-group">
                <select class="form-control" name="user_id">
                  @foreach($users as $user)
                    <option value="{{$user->id}}">{{$user->name}}</option>
                  @endforeach
                </select>
              </div>
            </div>

            <div class="col-lg-4">
              <div class="form-group">
                <select class="form-control" name="account_statement_types_id">
                  @foreach($statementTypes as $type)
                    <option value="{{$type->id}}">{{$type->description}}</option>
                  @endforeach
                </select>
              </div>
            </div>

            <div class="col-lg-3">
              <div class="form-group">
                <input type="text" class="form-control" name="ref_id" placeholder="Ref Id">
              </div>
            </div>

            <div class="col-lg-3">
              <div class="form-group">
                <input type="datetime" class="form-control" name="placed" placeholder="Colocada">
              </div>
            </div>
            
            <div class="col-lg-6">
              <div class="form-group">
                <input type="text" class="form-control" name="description" placeholder="Descrição">
              </div>
            </div>

            <div class="col-lg-3">
              <div class="form-group">
                <input type="text" class="form-control" name="average_odds" placeholder="Média probabilidades">
              </div>
            </div>

            <div class="col-lg-2">
              <div class="form-group">
                <input type="text" class="form-control" name="stopped" placeholder="Parada">
              </div>
            </div>

            <div class="col-lg-3">
              <div class="form-group">
                <input type="text" class="form-control" name="state" placeholder="Estado">
              </div>
            </div>

            <div class="col-lg-2">
              <div class="form-group">
                <input type="text" class="form-control" name="rd" placeholder="RD">
              </div>
            </div>

            <div class="col-lg-2">
              <div class="form-group">
                <input type="text" class="form-control" name="rc" placeholder="RC">
              </div>
            </div>

            <div class="col-lg-3">
              <div class="form-group">
                <input type="text" class="form-control" name="balance" placeholder="Saldo">
              </div>
            </div>

          </div>
          <div class="box-footer">
            <input type="submit" class="btn btn-success" value="Salvar">
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript" src="/template/plugins/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<link rel="stylesheet" href="/template/plugins/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />

<script type="text/javascript">
    $(function () {
        $('input[type=datetime]').datetimepicker({
          format: 'YYYY-MM-DD H:mm'
        });
    });
</script>
@endsection