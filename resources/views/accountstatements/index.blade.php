@extends('layout.panel')

@section('content')
<div class="content-header"></div>
<div class="content">
  <div class="row">
    <div class="col-lg-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Statements</h3>
          <div class="box-tools pull-right">
            <a href="{{route('accountstatements.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i></a>
          </div>
        </div>
        <div class="box-body no-padding">
          <table class="table table-responsive">
            <tbody>
              <tr>
                <th>Fixada</th>
                <th>Ref Id</th>
                <th>Colocada</th>
                <th>Descrição</th>
                <th>Média probabilidades</th>
                <th>Parada</th>
                <th>Estado</th>
                <th>RD<br>(€)</th>
                <th>RC<br>(€)</th>
                <th>Saldo<br>(€)</th>

              </tr>
              @foreach($statements as $statement)
                <tr>
                  <td>{{$statement->createdFormated()}}</td>
                  <td>{{$statement->ref_id}}</td>
                  <td>{{$statement->placedFormated()}}</td>
                  <td>{{$statement->description}}</td>
                  <td>{{number_format($statement->average_odds,2,',','.')}}</td>
                  <td>{{number_format($statement->stoped,2,',','.')}}</td>
                  <td>{{$statement->state}}</td>
                  <td>{{number_format($statement->rd,2,',','.')}}</td>
                  <td>{{number_format($statement->rc,2,',','.')}}</td>
                  <td>{{number_format($statement->balance,2,',','.')}}</td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <div class="box-footer">
          {{$statements->links()}}
        </div>
      </div>
    </div>
  </div>
</div>
@endsection