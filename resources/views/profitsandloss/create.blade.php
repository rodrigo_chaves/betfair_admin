@extends('layout.panel')

@section('content')
<div class="content-header"></div>
<div class="content">
  <div class="row">
    <div class="col-lg-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title"> Add Profits And Loss</h3>
        </div>
        <form role="form" action="{{route('profitsandloss.store')}}" method="post">
          {{ csrf_field() }}
          <div class="box-body">
            <div class="col-lg-4">
              <div class="form-group">
                <input type="text" class="form-control" name="market" placeholder="Market">
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <input type="datetime" class="form-control" name="start_time" placeholder="Start time">
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <input type="datetime" class="form-control" name="resolution_date" placeholder="Resolution time">
              </div>
            </div>
            <div class="col-lg-2">
              <div class="form-group">
                <input type="text" class="form-control" name="profit_prejudice" placeholder="Prejudice">
              </div>
            </div>
            
            
            <div class="form-group"></div>
            <div class="form-group"></div>
          </div>
          <div class="box-footer">
            <input type="submit" value="Salvar" class="btn btn-success">
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript" src="/template/plugins/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<link rel="stylesheet" href="/template/plugins/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />

<script type="text/javascript">
    $(function () {
        $('input[type=datetime]').datetimepicker({
          format: 'YYYY-MM-DD H:mm'
        });
    });
</script>

@endsection