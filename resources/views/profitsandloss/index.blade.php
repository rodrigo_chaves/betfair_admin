@extends('layout.panel')

@section('content')
<div class="content-header"></div>
<div class="content">
  <div class="row">
    <div class="col-lg-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Profits And Loss</h3>
          <div class="box-tools pull-right">
            <a href="{{route('profitsandloss.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i></a>
          </div>
        </div>
        <div class="box-body no-padding">
          <table class="table table-responsive">
            <tbody>
              <tr align="center">
                <th align="left">Mercado</th>
                <th>Hora de inicio</th>
                <th>Data da última resolução</th>
                <th>Lucro/prejuízo (€)</th>
              </tr>
              @foreach($profits as $profit)
                <tr>
                  <td> {{$profit->market}}</td>
                  <td>{{$profit->start_timeFormated()}}</td>
                  <td>{{$profit->resolution_dateFormated()}}</td>
                  <td>{{$profit->profit_prejudice}}</td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <div class="box-footer">
          {{$profits->links()}}
        </div>
      </div>
    </div>
  </div>
</div>
@endsection